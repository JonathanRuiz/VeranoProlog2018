(laaoad "graph-util")

(defparameter *congestion-city-nodes* nill)
(defparameter *congestion-city-edges* nill)
(defparameter *visited-nodes* nill)
(defparameter *node-num* 30)
(defparameter *edge-num* 45)
(defparameter *worm-num* 3)
(defparameter *cop-odds* 15)

;;Generation random edges 

(defun random-node()
	(1+ (random *node-num*)))

(defun edge-pair (a b)
	(unless (eql a b)
		(list (cons a b)(cons b a))))

(defun make-edge-list ()
	(apply #'append (loop repeat *edge-num*
		collect (edge-pair (random-node) (random-node)))))

(make-edge-list)
((16 . 20) (20 . 16) (9 . 3) (3 . 9) (25 . 18) (18 . 25) (30 . 29) (29 . 30)
(26 . 13) (13 . 26) (12 . 25) (25 . 12) (26 . 22) (22 . 26) (30 . 29) (29 .
30) (3 . 14) (14 . 3) (28 . 6) (6 . 28) (4 . 8) (8 . 4) (27 . 8) (8 . 27) (3 .
30) (30 . 3) (25 . 16) (16 . 25) (5 . 21) (21 . 5) (11 . 24) (24 . 11) (14 .
1) (1 . 14) (25 . 11) (11 . 25) (21 . 9) (9 . 21) (12 . 22) (22 . 12) (21 .
11) (11 . 21) (11 . 17) (17 . 11) (30 . 21) (21 . 30) (3 . 11) (11 . 3) (24 .
23) (23 . 24) (1 . 24) (24 . 1) (21 . 19) (19 . 21) (25 . 29) (29 . 25) (1 .
26) (26 . 1) (28 . 24) (24 . 28) (20 . 15) (15 . 20) (28 . 25) (25 . 28) (2 .
11) (11 . 2) (11 . 24) (24 . 11) (29 . 24) (24 . 29) (18 . 28) (28 . 18) (14 .
15) (15 . 14) (16 . 10) (10 . 16) (3 . 26) (26 . 3) (18 . 9) (9 . 18) (5 . 12)
(12 . 5) (11 . 18) (18 . 11) (20 . 17) (17 . 20) (25 . 3) (3 . 25))


(defun direct-edges (node edge-list)
	(remove-if-not (lambda (x)
		(eql (car x) node))
	edge-list))

(defun get-connected (node edge-list)
	(let ((visited nill))
		(labels ((traverse (node)
			(unless (member node visited)
				(push node visited)
				(mapc (lambda (edge)
					(traverse (cdr edge)))
				(direct-edges node edge-list)))))
		(traverse node))
		visited))

(defun find-islands (nodes edge-list)
 (let ((islands nil))
 (labels ((find-island (nodes)
 (let* ((connected (get-connected (car nodes) edge-list))
 (unconnected (set-difference nodes connected)))
 (push connected islands)
 (when unconnected
 (find-island unconnected)))))
 (find-island nodes))
 islands))

(defun connect-with-bridges (islands)
 (when (cdr islands)
 (append (edge-pair (caar islands) (caadr islands))
 (connect-with-bridges (cdr islands)))))
 
 (defun connect-all-islands (nodes edge-list)
 (append (connect-with-bridges (find-islands nodes edge-list)) edge-list))

 (defun make-city-edges ()
(let* ((nodes (loop for i from 1 to *node-num*
collect i))
 (edge-list (connect-all-islands nodes (make-edge-list)))
 (cops (remove-if-not (lambda (x)
 (zerop (random *cop-odds*)))
 edge-list)))
 (add-cops (edges-to-alist edge-list) cops)))

(defun edges-to-alist (edge-list)
 (mapcar (lambda (node1)
 (cons node1
(mapcar (lambda (edge)
 (list (cdr edge)))
 (remove-duplicates (direct-edges node1 edge-list)
 :test #'equal))))
 (remove-duplicates (mapcar #'car edge-list))))

(defun add-cops (edge-alist edges-with-cops)
 (mapcar (lambda (x)
 (let ((node1 (car x))
 (node1-edges (cdr x)))
 (cons node1
 (mapcar (lambda (edge)
 (let ((node2 (car edge)))
	(if (intersection (edge-pair node1 node2)
 edges-with-cops
 :test #'equal)
 (list node2 'cops)
 edge)))
 node1-edges))))
 edge-alist))

(defun neighbors (node edge-alist)
 (mapcar #'car (cdr (assoc node edge-alist))))
(defun within-one (a b edge-alist)
 (member b (neighbors a edge-alist)))

(defun within-two (a b edge-alist)
 	(or (within-one a b edge-alist)
 (some (lambda (x)
 	(within-one x b edge-alist))
	 (neighbors a edge-alist))))

(defun make-city-nodes (edge-alist)
	(let ((wumpus (random-node))
 (glow-worms (loop for i below *worm-num*
 collect (random-node))))
 (loop for n from 1 to *node-num*
 collect (append (list n)
 (cond ((eql n wumpus) '(wumpus))
 ((within-two n wumpus edge-alist) '(blood!)))
 (cond ((member n glow-worms)
 '(glow-worm))
	((some (lambda (worm)
 (within-one n worm edge-alist))
 glow-worms)
 '(lights!)))
 (when (some #'cdr (cdr (assoc n edge-alist)))
 '(sirens!))))))

(defun new-game ()
 (setf *congestion-city-edges* (make-city-edges))
 (setf *congestion-city-nodes* (make-city-nodes *congestion-city-edges*))
 (setf *player-pos* (find-empty-node))
 (setf *visited-nodes* (list *player-pos*))
 (draw-city))