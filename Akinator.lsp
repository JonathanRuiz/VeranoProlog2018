(defparameter *todo*
    '(
        (game_of_thrones
            (Es_un_animal
                (Es_un_lobo_huargo
                    (De_color_blanco
                        (Fantasma)
                    )
                    (No_es_blanco
                        (Es_macho
                            (Salvo_la_vida_de_Bran
                                (Verano)
                            )
                            (Peleo_en_la_guerra
                                (viento_gris)
                            )
                            (es_de_Rykcon
                                (Peludo)
                            )
                        )
                        (Es_hembra
                            (Su_nombre_es_en_honor_a_una_guerrera
                                (Nymeria)
                            )
                            (La_mas_joven_y_bonita
                                (Dama)
                            )
                        )
                    )
                )
                (Es_un_dragon
                    (Esta_muerto
                        (Viseryon)
                    )
                    (Es_Rojo
                        (Drogon)
                    )
                    (No_ha_muerto
                        (Rhaegal)
                    )
                )
            )
            (Es_hombre
                (Pertenece_a_la_casa_Stark
                    (Esta_vivo
                        (Es_bastardo
                            (Jon_Snow)
                        )
                        (Es_el_cuervo_de_tres_ojos
                            (Bran_Stark)
                        )
                    )
                    (Esta_muerto
                        (Es_de_la_primera_generacion
                            (Asesinado_por_el_rey_loco
                                (Brandon_Stark)
                            )
                            (Hermano_de_la_guardia_nocturna
                                (Benjen_Stark)
                            )
                            (Murio_por_ser_honorable_y_correcto
                                (Eddard_Stark)
                            )
                        )
                        (Segunda_Generacion
                            (Muerto_en_la_boda_roja
                                (Robb_Stark)
                            )
                            (Muerto_en_la_batalla_de_bastardos
                                (Rickon_Stark)
                            ) 
                        )
                    )
                )
                (Pertenece_a_la_casa_Targaryen
                    (Es_de_la_generacion_actual
                        (Murio_en_la_batalla_del_usurpador
                            (Rhaegard_Targaryen)
                        )
                        (Murio_en_la_batalla_del_usurpador
                            (Es_hijo_de_Daenerys
                                (Raegon_Targaryen)
                            )
                            (Apodado_rey_mendigo
                                (Viserys)
                            )
                        )
                    )
                    (Es_de_la_generacion_anterior
                        (Esta_loco
                            (Aeris_II_Targaryen)
                        )
                        (Maestre_de_la_guardia_nocturna
                            (Aemon_II_Targaryen)
                        )
                    )
                )
                (Pertenece_a_la_casa_Lannister
                    (Esta_vivo
                        (Es_un_enano
                            (Tyrion_lannister)
                        )
                        (Le_amputaron_una_mano_ademas_de_ser_insestuoso
                            (Jaime_lannister)
                        )
                        (Tiene_un_hermano_asquerosamente_rico
                            (Kevan_lannister)
                        )
                    )
                    (Esta_muerto
                        (Asesinado_por_su_hijo_por_condenarlo_a_muerte_y_acostarse_con_su_mujer
                            (Tywin_lannister)
                        )
                        (Asesinado_por_una_piromana
                            (Lancel_lannister)
                        )
                    )
                )
                (Pertenece_a_la_casa_Baratheon
                    (Es_de_la_primera_generacion
                        (Asesinado_por_un_jabali
                            (Robert_Batatheon)
                        )
                        (Asesinado_por_seguir_a_una_mujer_religiosa_loca_obsesionada_con_el_señor_de_la_luz
                            (Stanis_Baratheon)
                        )
                        (Asesinado_por_su_hermano
                            (Renly_Baratheon)
                        )
                    )
                    (Es_de_la_generacion_actual
                        (Es_bastardo_aunque_es_hijo_de_sangre_de_Robert_Batatheon
                            (Gendri) 
                        )
                        (Envenendo_y_odiado_por_ser_el_personaje_mas_odioso_de_todos_los_tiempos
                            (Joffrey_Baratheon)
                        )
                        (Se_Suicido_porque_su_madre_quemo_a_su_esposa
                            (Tomen_Baratheon)
                        )
                    )
                )
                (Pertenece_a_la_casa_Tyrell
                    (Señor_de_Higarden
                        (Mace_Tyrell)
                    )
                    (Quemado_por_una_piromana_con_fuego_salvaje
                        (Loras_Tyrell)
                    )
                )
            )
            (Es_Mujer
                (Pertenece_a_la_casa_Stark
                    (Esta_viva
                        (Hombre_sin_rostro
                            (Arya_Stark)
                        )
                        (Laydy_de_winterfell
                            (Sansa_Stark)
                        )
                    ) 
                    (Esta_muerto
                        (Hermana_de_Eddar_Strar
                            (Lyanna_Stark)
                        )
                        (Esposa_de_Eddar_Stark
                            (Catelyn_Tully)
                        )
                    )
                )
                (Pertenece_a_la_casa_Lannister
                    (Es_piromana_ademas_de_insestuosa
                        (Cersei_Lannister)
                    )
                    (Prima_y_esposa_de_Tywin_Lannister
                        (Joanne_Lanister)
                    )
                )
                (Pertenece_a_la_casa_Targaryen
                    (Es_la_primera_de_su_nombre_la_que_no_arde_Reina_de_Meereen_Reina_de_los_andalos_y_los_primeros_hombres_Khaleesi_del_gran_mar_de_hierba_rompedora_de_cadenas_y_madre_de_dragones
                        (Daenerys_Targaryen)
                    )
                    (Madre_de_Daenerys
                        (Raella_Targaryen)
                    )
                )
                (Pertenece_a_la_casa_Baratheon
                    (Envenenada_por_el_odio_de_los_Martell_hacia_los_lannister
                        (Myrcella_Baratheon)
                    )
                    (Quemada_por_ordenes_de_su_propio_padre
                        (Shireen_Baratheon)
                    )
                    (Se_suicido_por_ver_como_quemaban_a_su_hija
                        (Selyse_Florent)
                    )
                )
                (Pertenece_a_la_casa_Tyrell
                    (Apodada_reyna_de_las_espinas
                        (Lady Olena)
                    )
                    (Quemada_por_una_piromana_con_fuego_salvaje
                        (Margaery_Tyrell)
                    )
                )
            )
        )
        (steven_universe
            (Humano
                (Masculino
                    (La_gran_rosquilla
                        (Lars_Barriga)
                    )
                    (Auto_lavado
                        (Greg_Universe)
                    )
                    (DJ
                        (Crema_Agria)
                    )
                    (Rarito
                        (Cebolla)
                    )
                    (Presumido
                        (Kevin)
                    )
                    (Blog_Cosas_Locas_de_Ciudad_Playa
                        (Ronaldo_Fryman)
                    )
                    (Exmanager_de_Greg
                        (Marty)
                    )
                    (Piloto
                        (Andy_DeMayo)
                    )
                    (Cartero
                        (Jamie)
                    )
                    (Marinero
                        (Yellowtail)
                    )
                    (Hijo_de_exalcalde
                        (Buck_Dewey)
                    )
                    (Exalcalde
                        (Bill_Dewey)
                    )
                    (Padre_de_Connie
                        (Sr_Maheswaran)
                    )
                    (Parque_de_Diversiones_de_Ciudad_Playa
                        (Sr_Sonrisas)
                    )
                    (Freidor
                        (Peedee_Fryman)
                    )
                    (Manager_de_Pizzas_de_Pescado
                        (Kofi_Pizza)
                    )
                    (Jefe_en_Caminata_Frita
                        (Fryman)
                    )

                )
                (Femenino
                    (Estudiante
                        (Connie_Maheswaran)
                    )
                    (La_gran_rosquilla
                        (Sadie_Miller)
                    )
                    (Pintora
                        (Vidalia)
                    )
                    (Familia_Pizza
                        (Chicos_Geniales
                            (Jenny_Pizza)
                        )
                        (Anciana_Alcalde
                            (Nanefua_Pizza)
                        )
                        (Cajera_en_Pizzas_de_Pescado
                            (Kiki_Pizza)
                        )
                    )
                    (Doctora
                        (Dra_Priyanka_Maheswaran)
                    )
                    (Cartera
                        (Barb_Miller)
                    )
                )
            )
            (Gema
                (Diamante
                    (Superior
                        (White_Diamond)
                    )
                    (Amorosa_Despreocupada
                        (Pink_Diamond)
                    )
                    (Triste_Depresiva
                        (Blue_Diamond)
                    )
                    (Malumorada
                        (Yellow_Diamond)
                    )
                )
                (Gemas_de_Cristal
                    (Protectora
                        (Perla)
                    )
                    (Lider
                        (Garnet)
                    )
                    (Divertida_Despreocupada
                        (Amatista)
                    )
                    (Controla_Metal
                        (Peridot)
                    )
                    (Cedio_su_vida
                        (Rose_Cuarzo)
                    )
                    (Humano_Gema
                        (Steven_Cuarzo_Universe)
                    )
                    (Problemas_emocionales
                        (Lapislazuli)
                    )
                    (Crea_Armas
                        (Bismuto)
                    )

                )
                (Fusion
                    (Garnet_Perla_Amatista
                        (Alejandrita)
                    )
                    (Amatista_Perla
                        (Opalo)
                    )
                    (Amatista_Steven
                        (Smoky_Cuarzo)
                    )
                    (Amatista_Garnet
                        (Sugalite)
                    )
                    (Steven_Connie
                        (Stevonnie)
                    )
                    (Garnet_Perla
                        (Sardonyx)
                    )
                    (Jaspe_Lapiz
                        (Malachite)
                    )
                )
            )
            (Animal
                (Mascotas
                    (Calabaza
                        (Pumpkin)
                    )
                    (Abre_portales
                        (Leon_rosa)
                    )
                    (Gato
                        (Gata_Steven)
                    )
                    (Hijo_de_Steven
                        (Cabra_Steven_Jr)
                    )
                )
                (Comida_de_Leon
                    (Basilisco_de_Cristal)
                )
            )
        )
        (lol
            (Masculino
                (Ionia
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (format t "Este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (varus)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (fighter
                                        (Forjido
                                            (yasuo))
                                        
                                        (Zed)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (Ser_Con_Mascara
                                            (jhin))
                                        
                                        (varus)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (kennen)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (shen)
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Mono
                                            (wukong))
                                        (kayn)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (un_hamster
                                            (kennen))
                                        
                                        (rakan)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (shen)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (rakan)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Un_Ser_Oscuro
                                            (kayn))
                                        (Ciego
                                            (lee_sin))
                                        (Mono
                                            (wukong))
                                        (master_yi)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                    
                )
            (Runaterra
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Dragon
                                            (Aurelion_sol))
                                        (de_fuego
                                            (Brand))
                                        (un_Pescado
                                            (Fizz))
                                        (Ryze)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Fizz)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (Malphite)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Ryze)
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Fizz)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Gnar)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (Gnar)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (fighter
                                        (Un_Ser_demoniaco
                                            (Aatrox))
                                        (Jax)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (Una_Vaca
                                            (Alistar))
                                        (Un_Bagre
                                            (Tahm_Kench))
                                        (Malphite)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (Una_Vaca
                                            (Alistar))
                                        (Un_Arbol
                                            (Ivern))
                                        
                                        (Zilean)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Un_Espantapajaros
                                            (Fiddlesticks))
                                        (de_fuego
                                            (Brand))
                                        (un_ser_Mitico
                                            (Bard))

                                        (Zilean)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Un_Bagre
                                            (Tahm_Kench))
                                        (Malphite)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Bard)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (Gordo
                                            (Gragas))
                                        
                                        (Malphite)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (Ivern)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Un_Espantapajaros
                                            (Fiddlesticks))
                                        
                                        (Gragas)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )

                                )
                                (ad
                                    (tanque
                                        (Malphite)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (Kindred)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (fighter
                                        (Un_Ser_demoniaco
                                            (Aatrox))
                                        (Un_Ser_Oscuro
                                            (Nocturne))
                                        (Un_Arlequin
                                            (Shaco))
                                        (Un_Gato
                                            (Rengar))
                                        (Jax)

                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                    
                )
            (freiljord
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Vikingo
                                            (Olaf))
                                        (Troll
                                            (Trundle))
                                        (Un_Oso
                                            (Volibear))
                                        (Ornn)

                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Vikingo
                                            (Olaf))
                                        (Troll
                                            (Trundle))
                                        
                                        (Tryndamere)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (Nunu)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Nunu)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Braum)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (Udyr)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Udyr)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Vikingo
                                            (Olaf))
                                        (Troll
                                            (Trundle))
                                        (Un_Oso
                                            (Volibear))
                                        (Udyr)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Vikingo
                                            (Olaf))
                                        (Troll
                                            (Trundle))
                                        (Fuerte_De_Un_Brazo
                                            (Tryndamere))
                                        
                                        (Udyr)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                    
                )
                
            )
            (femenino
                (Ionia
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (Zorra
                                            (Ahri))
                                        
                                        (Karma)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Zorra
                                            (Ahri))
                                        (Un_Ser_Oscuro
                                            (Syndra))
                                        (Karma)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Ninja
                                            (Akali))
                                        
                                        (Irelia)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (Xayah)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (Ninja
                                            (Akali))
                                        
                                        (Irelia)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Akali)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                    (fighter
                                        (Irelia)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (Cabra
                                            (Soraka))
                                        (Karma)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Zorra
                                            (Ahri))
                                        (Un_Ser_Oscuro
                                            (Syndra))
                                        (Karma)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                    
                )
            (Runaterra
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (Niña_pequeña
                                            (Annie))
                                        
                                        (Morgana)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Niña_pequeña
                                            (Annie))
                                        (planta
                                            (Zyra))
                                        (Morgana)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Kayle)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (Kayle)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (Sirena
                                            (Nami))
                                        (Kayle)
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Niña_pequeña
                                            (Annie))
                                        (planta
                                            (Zyra))
                                        (Un_Ser_Oscuro
                                            (Morgana))
                                        (Kayle)
                                        
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (Nidalee)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (leopardo
                                            (Nidalee))
                                        (Angel
                                            (Kayle))
                                        (Evelynn)
                                        
                                        
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Kayle)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (Kindred)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                    
                )
            (freiljord
                    
                        (mid
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (Anivia)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (Ave
                                            (Anivia))
                                        
                                        (Lissandra)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (adc
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (Ashe)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                )
                            
                        )
                        (top
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (soporte
                            
                                (ap
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (Anivia)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                        (jungla
                            
                                (ap
                                    (tanque
                                        (Sejuani)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (soporte
                                        (Sejuani)
                                        (format t "entonces no existe con las caracteristicas que dices")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                                (ad
                                    (tanque
                                        (format t "este personaje no existe")
                                    )
                                    (soporte
                                        (format t "este personaje no existe")
                                    )
                                    (mago
                                        (format t "este personaje no existe")
                                    )
                                    (tirador
                                        (format t "este personaje no existe")
                                    )
                                )
                            
                        )
                    
                )
            )
        )
        (star_wars
            (Humano
          (Masculino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Coruscant
                    (Qui_Gon_Jinn)
                  )
                  (Stewjon
                    (Obi_Wan_Kenobi)
                  )
                  (Tatooine
                    (Anakin_Skywalker)
                  )
                  (Haruun_kal
                    (Mace_Windu)
                  )
                )
              )
            )
            (Republica_Galactica
              (Senado_Galactico
                (Naboo
                  (Sheev_Palpatine)
                )
                (Canciller_Supremo
                  (Coruscant
                    (Finis_Valorum)
                  )
                )
              )
            )
            (Sith
              (Naboo
                (Darth_Sidious)
              )
            )
          )
          (Femenino
            (Republica_Galactica
              (Senado_Galactico
                (Naboo
                  (Padme_Amidala)
                )
              )
            )
            (Tatooine
              (Shmi_Skywalker)
            )
          )
        )
        (Gungan
          (Republica_Galactica
              (Senado_Galactico
                (Naboo
                  (JarJar_Binks)
                )
              )
          )
        )
        (Robot
          (Masculino
            (Tatooine
              (C3PO)
            )
            (Naboo
              (R2_D2)
            )
          )
        )
        (Especie_de_Yoda
          (Masculino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Yoda)
                )
              )
            )
           )
           (Femenino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Yaddle)
                )
              )
            )
           ) 
        )
        (Toydariano
          (Masculino
            (Toydaria
              (Watto)
            )
          )
        )
        (Zabrak
          (Siths
            (Dathomir
              (Darth_Maul)
            )
          )
        )
        (Dug
          (Masculino
            (Malastere
              (Sebulba)
            )
          )
        )
        (Hutt
          (Masculino
            (Gran_consejo_de_los_Hutt
              (Nal_Hutta
                (Jabba_The_Hutt)
              )
            )
          )
        )
        (Tholothiana
          (Femenino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Coruscant
                    (Adi_Galia)
                  )
                )
              )
            )
          )
        )
        (Kel_Dor
          (Masculino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Dorin
                    (Plo_Koon)
                  )
                )
              )
            )
          )
        )
        (Cereano
          (Masculino
            (Jedi
              (Republica_Galactica
                (Orden_Jedi
                  (Cerea
                    (Ki_Adi_Mundi)
                  )
                )
              )
            )
          )
        )
        )
    )
)


(defun hacer_pregunta (lista)
    (if (= (length lista) 1)
        (format t "Tu personaje es ~s " (car(car lista)))
        (format t "¿~s? s/n " (car (car lista)))
    )
)

(defun recorre(arbol)
    (when arbol
        (hacer_pregunta arbol)
        (unless (= (length arbol) 1)
            (set 'respuesta (read))
            (if (eq respuesta 's)
                (set 'lst (cdr (car arbol)))
                (if (> (length arbol) 2)
                    (set 'lst (cdr arbol))
                    (set 'lst (cdr (car (cdr arbol))))
                )
            )
            (recorre lst)
        )
    )
)
