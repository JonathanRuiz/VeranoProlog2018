;;25.1- recibe lista y devuelve una con el primero y el ultimo numero

;;25.2- tres parametros que es la fecha y retorna si es correcta la fecha

;;25.3- Define una funcion que se llama aplanar qu recibe como argumentos una expresion simbolica  y elimine todo tipo de parentesis y devuelva una lista con lo que hay dentro de los parentesis
;;ejemplo: ((8)8)(8 8 8 9)))

;;25.4- Definir una funcion que reciba como argumento una lista de numeros y devuelva una lista cuyos elementos sean los cuadrados de los anteriores


(defun first&last (lista) 
	(list (car lista) (car (last lista))))
	
defun fecha-correct (dia mes year)
  (and (> mes 0) (<= mes 12)
       (> dia 0)
       (let ((max (cond
                   ((= mes 2) 28)
                   ((evenp mes) 30)
                   ))
        (<= dia max))))

(defun aplanar (lista)
  (when (consp lista)
    (let ((h (car lista)) (tl (cdr lista)))
      (append  (if (consp h) (aplanar h)
                 (list h))
               (aplanar tl)))))

(defun aplanar2 (x)
  (if (consp x)
      (apply #'append (mapcar #'aplanar2 x))
    (list x)))

(defun cuadrados (lista) (mapcar (lambda (x) (expt x 2)) lista))