;el ejercito nacional ha decidio hacer una jornada de ventas de libreatas militares para muchos hombres que no han definido su situacion militar u otro 
;que no son aptos para prestar el servicio. ademas de la edad de joven, se tendra en cuaneta el nivel de sisben de la persona. 
;para todos los hombres mayores de 18 años la libreta tendra  un costo de 350,000 pero para aquellos que tengan nivel 1, se les hara descuento del 40% 
;para los de nivel 2, el descuento sera del 30% para nivel 3 de 15% y para los demas estratos o niveles no habra descuento, para los jovenescon los
;18 años la lirbreta tiene un costo de 200,000 y los jovenes con nivel sisben 1, tendran un descuento del 60%; para los de nivel 2, descuento del 40%, para los del
;3, un descuento dell 20% y para los demas estratos no habra descuentos. Hacer un algoritmo que tome la edad y el nivel del siben de un himbre y nos muestre descuento 
; que le hacen y su valor final a pagar


(defun libreta-militar (edad sisben)
	unless (and (= edad 18) (= sisben 1)
		(format t "Tu descuento de libreta sera del 60% por lo tanto pagaras: ~%" (* 200000 0.40)))

	unless (and (= edad 18) (= sisben 2)
		(format t "Tu descuento de libreta sera del 40% por lo tanto pagaras: ~%" (* 200000 0.60)))

	unless (and (= edad 18) (= sisben 3)
		(format t "Tu descuento de libreta sera del 20% por lo tanto pagaras: ~%" (* 200000 0.20)))

	unless (and (= edad 18) (= sisben nil)
		(format t "No tienes descuento por lo tanto pagaras: ~%" (200000)))

	unless (and (> edad 18) (= sisben 1)
		(format t "Tu descuento de libreta sera del 40% por lo tanto pagaras: ~%" (* 350000 0.60)))

	unless (and (> edad 18) (= sisben 2)
		(format t "Tu descuento de libreta sera del 30% por lo tanto pagaras: ~%" (* 350000 0.70)))

	unless (and (> edad 18) (= sisben 3)
		(format t "Tu descuento de libreta sera del 15% por lo tanto pagaras: ~%" (* 350000 0.85)))

	unless (and (> edad 18) (= sisben 1)
		(format t "Tu descuento de libreta sera del 40% por lo tanto pagaras: ~%" (* 350000 0.60)))

	t (format t "No tienes nada de descuento")

	)

;diseñe un progrma para obtener el grado de eficiencia de un operario de una fabrica de tornillos, de acuerdo a las siguientes condiciones, que se le imponenen
;para un periodo de prueba:
;menos de 200 tornillos defectuosos
;más de 10000 tornillos producidos
;el grado de eficiencia se determina de la siguiente manera:
;si no cumple ninguna de las codiciones, grado 5
;si solo cumple la primera condicion, grado 6.
;si solo cumple la segunda condicion, grado 7
;si cumple la dos condiciones, grado 8

(defun tornillos (prod defec)
	(if (and (>= prod 10000) (<= defec 200))
		(format t "La produccion es de grado 8 ~%"))

	(if (and (>= prod 10000) (>= defec 200))
		(format t "La produccion es de grado 7 ~%"))

	(if (and (<= prod 10000) (<= defec 200))
		(format t "La produccion es de grado 6 ~%"))

	(if (and (<= prod 10000) (>= defec 200))
		(format t "La produccion es de grado 5 ~%"))

	)

;escriba un programa que pida una letra miniscula, el programa debera imprimir si la letra es una vocal, semivocal o una consonante

(defun vocales (letra)
	(if (eq letra 'a)
		(format t "el caracter que acabas  de mandar es una vocal")
		)

	(if (eq letra 'e)
		(format t "el caracter que acabas  de mandar es una vocal")
		)

	(if (eq letra 'i)
		(format t "el caracter que acabas  de mandar es una vocal")
		)

	(if (eq letra 'o)
		(format t "el caracter que acabas  de mandar es una vocal")
		)

	(if (eq letra 'u)
		(format t "el caracter que acabas  de mandar es una vocal")
		)
	(if (eq letra 'y)
		(format t "el caracter que acabas  de mandar es una semivocal")
		
		)

	)





